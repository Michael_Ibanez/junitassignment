package bearman;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

import food.Food;
import food.Pizza;
import food.Taco;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class BearmanTest {

	private static Bearman foodEater;
	private static Bearman pizzaEater;
	private static Bearman tacoEater;
	
	private static Food food;
	private static Food pizza;
	private static Food taco;
	
	@BeforeAll
	public static void createBearmen() {
		foodEater = new Bearman();
		pizzaEater = new PizzaEatingBearman();
		tacoEater = new TacoEatingBearman();
	}
	
	@BeforeAll
	public static void createFoods() {
		food = new Food();
		pizza = new Pizza();
		taco = new Taco();
	}
	
	@Test
	public void testBearmanHunger() {
		// foodEater should be hungry since he hasn't eaten anything
		assertTrue(foodEater.isHungry());
				
		// foodEater should not be hungry after he eats plain food
		food.feedBearman(foodEater);
		assertFalse(foodEater.isHungry());
				
				
		// foodEater should not be hungry after eating pizza
		pizza.feedBearman(foodEater);
		assertFalse(foodEater.isHungry());
				
		// foodEater should not be hungry after eating taco
		taco.feedBearman(foodEater);
		assertFalse(foodEater.isHungry());
	}
	
	@Test
	public void testPizzaEatingBearmanHunger() {
		// Pizza eater should be hungry since he hasn't eaten anything
		assertTrue(pizzaEater.isHungry());
		
		// Pizza eater should still be hungry after he eats plain food
		pizzaEater.eat(food);
		assertTrue(pizzaEater.isHungry());
		
		
		// pizzaEater shouldn't be hungry after eating pizza
		pizzaEater.eat(pizza);
		assertFalse(pizzaEater.isHungry());
		
		// pizzaEater should be hungry after eating taco
		pizzaEater.eat(taco);
		assertTrue(pizzaEater.isHungry());
		
	}
	
	@Test
	public void testTacoEatingBearmanHunger() {
		// tacoEater should be hungry since he hasn't eaten anything
		assertTrue(tacoEater.isHungry());
				
		// tacoEater should still be hungry after he eats plain food
		tacoEater.eat(food);
		assertTrue(tacoEater.isHungry());
				
				
		// tacoEater should be hungry after eating pizza
		tacoEater.eat(pizza);
		assertTrue(tacoEater.isHungry());
				
		// pizzaEater should not be hungry after eating taco
		tacoEater.eat(taco);
		assertFalse(tacoEater.isHungry());
				
	}
}
